<!DOCTYPE html>
<html>
<head>
	<title>JobFair</title>
	<link rel="stylesheet" type="text/css" href="css/style_keluarga.css">
</head>
<body>

<?php 
include 'koneksi.php';
 
// mengaktifkan session
session_start();
 
// cek apakah user telah login, jika belum login maka di alihkan ke halaman login
if($_SESSION['status'] !="login"){
	header("location:index.php");
}
 
// menampilkan pesan selamat datang
echo "<div class='nav-bar'>";
echo 	"<ul>";
echo 		"<li class='dropdown'><a href='#'>".$_SESSION['email']."</a>";
echo 			"<ul class='isi-dropdown'>";
echo 				"<li><a href='logout.php'>"."Logout"."</a><li>";
echo 				"<li><a href='setting.php'>"."Setting"."</a><li>";
echo 			"</ul>";
echo 		"</li>";
echo 	"</ul>";
echo "</div>";


 
?>

<?php
	$strSQL = "SELECT users.id, users.email, users.nama, profil.id, profil.foto FROM users INNER JOIN profil ON users.id = profil.id WHERE email='$_SESSION[email]' ";
			$query = mysql_query ($strSQL) or die ("query salah");
?>

<div class="left_bar">
	<h3>JobFair</h3>
	<ul>
		<div class="foto_profil">
			<il>
				<?php
					while ($row = mysql_fetch_array($query)) {
						$cek_foto = $row ['foto'];
						$tempat_foto = 'foto/'.$row['foto']; 
						if ($cek_foto >= 1) {
							echo "<a href='setting.php'><img src='$tempat_foto'></a></br>";	
						}else{
							echo "<a href='setting.php'><img src='foto/blank.png'></a></br>";
						}
						echo "<a href='setting.php'>".$row ["nama"]."</a>";
					}
					?>		
			</il>
		</div>
	<il >
		<a class="menu2" href="home.php">My Profile</a>
	</il>
	<li>
		<a class="menu2" href="pendidikan_formal.php">Formal Education</a>
	</li>
	<li>
		<a class="menu2" href="pendidikan_non_formal.php">Non Formal Education</a>
	</li>
	<li>
		<a class="menu2" href="pengalaman_kerja.php">Experience</a>
	</li>
	<li>
		<a class="menu2" href="pengalaman_organisasi.php">Organization Activity</a>
	</li>
	<li>
		<a class="menu" href="keluarga.php">My Family</a>
	</li>
	
</div>
	<div class="hidden">Hidden</div>
<div class="tengah kotak_tengah">
	<div class="kotak_dalam">
		<b>My Family</b>

		<?php

			$strSQL2 = "SELECT users.id, users.email, keluarga.id, keluarga.nama_lengkap, keluarga.hubungan, keluarga.jenis_kelamin, keluarga.pendidikan_terakhir, keluarga.tempat_lahir, keluarga.tgl_lahir, keluarga.pekerjaan, keluarga.nama_lengkap2, keluarga.hubungan2, keluarga.jenis_kelamin2, keluarga.pendidikan_terakhir2, keluarga.tempat_lahir2, keluarga.tgl_lahir2, keluarga.pekerjaan2, keluarga.nama_lengkap3, keluarga.hubungan3, keluarga.jenis_kelamin3, keluarga.pendidikan_terakhir3, keluarga.tempat_lahir3, keluarga.tgl_lahir3, keluarga.pekerjaan3, keluarga.nama_lengkap4, keluarga.hubungan4, keluarga.jenis_kelamin4, keluarga.pendidikan_terakhir4, keluarga.tempat_lahir4, keluarga.tgl_lahir4, keluarga.pekerjaan4, keluarga.nama_lengkap5, keluarga.hubungan5, keluarga.jenis_kelamin5, keluarga.pendidikan_terakhir5, keluarga.tempat_lahir5, keluarga.tgl_lahir5, keluarga.pekerjaan5 FROM users INNER JOIN keluarga ON users.id = keluarga.id WHERE email='$_SESSION[email]' ";
			$query2 = mysql_query ($strSQL2) or die ("query salah");
			while ($row2 = mysql_fetch_array($query2)){
				$id2 = $row2 ['id'];

			echo "<a class='button_update' href=keluarga_update.php?id=".$id2.">"."Update"."</a>";
			echo "</div>";
			echo "<div>";
			echo 	"<table class='tabel' border='1'>";
			if ($row2["nama_lengkap"]) {
				echo "<tr>";
				echo "<th>Nama Lengkap</th>";
				echo "<th>Hubungan</th>";
				echo "<th>Jenis Kelamin</th>";
				echo "<th>Pendidikan Terakhir</th>";
				echo "<th>Tempat Lahir</th>";
				echo "<th>Tanggal Lahir</th>";
				echo "<th>Pekerjaan</th>";
				echo "</tr>";
				echo "<tr>";
				echo "<td>".$row2["nama_lengkap"]."</td>";
				echo "<td>".$row2["hubungan"]."</td>";
				echo "<td>".$row2["jenis_kelamin"]."</td>";
				echo "<td>".$row2["pendidikan_terakhir"]."</td>";
				echo "<td>".$row2["tempat_lahir"]."</td>";
				echo "<td>".date("d-M-Y", strtotime($row2 ["tgl_lahir"]))."</td>";
				echo "<td>".$row2["pekerjaan"]."</td>";
				echo "</tr>";
			}else{
				echo "Update datamu sekarang!";
			}
			if ($row2["nama_lengkap2"]) {
				echo "<tr>";
				echo "<td>".$row2["nama_lengkap2"]."</td>";
				echo "<td>".$row2["hubungan2"]."</td>";
				echo "<td>".$row2["jenis_kelamin2"]."</td>";
				echo "<td>".$row2["pendidikan_terakhir2"]."</td>";
				echo "<td>".$row2["tempat_lahir2"]."</td>";
				echo "<td>".date("d-M-Y", strtotime($row2 ["tgl_lahir2"]))."</td>";
				echo "<td>".$row2["pekerjaan2"]."</td>";
				echo "</tr>";
			}
			if ($row2["nama_lengkap3"]) {
				echo "<tr>";
				echo "<td>".$row2["nama_lengkap3"]."</td>";
				echo "<td>".$row2["hubungan3"]."</td>";
				echo "<td>".$row2["jenis_kelamin3"]."</td>";
				echo "<td>".$row2["pendidikan_terakhir3"]."</td>";
				echo "<td>".$row2["tempat_lahir3"]."</td>";
				echo "<td>".date("d-M-Y", strtotime($row2 ["tgl_lahir3"]))."</td>";
				echo "<td>".$row2["pekerjaan3"]."</td>";
				echo "</tr>";
			}
			if ($row2["nama_lengkap4"]) {
				echo "<tr>";
				echo "<td>".$row2["nama_lengkap4"]."</td>";
				echo "<td>".$row2["hubungan4"]."</td>";
				echo "<td>".$row2["jenis_kelamin4"]."</td>";
				echo "<td>".$row2["pendidikan_terakhir4"]."</td>";
				echo "<td>".$row2["tempat_lahir4"]."</td>";
				echo "<td>".date("d-M-Y", strtotime($row2 ["tgl_lahir4"]))."</td>";
				echo "<td>".$row2["pekerjaan4"]."</td>";
				echo "</tr>";
			}
			if ($row2["nama_lengkap5"]) {
				echo "<tr>";
				echo "<td>".$row2["nama_lengkap5"]."</td>";
				echo "<td>".$row2["hubungan5"]."</td>";
				echo "<td>".$row2["jenis_kelamin5"]."</td>";
				echo "<td>".$row2["pendidikan_terakhir5"]."</td>";
				echo "<td>".$row2["tempat_lahir5"]."</td>";
				echo "<td>".date("d-M-Y", strtotime($row2 ["tgl_lahir5"]))."</td>";
				echo "<td>".$row2["pekerjaan5"]."</td>";
				echo "</tr>";
			}
			echo 	"</table>";
			echo "</div>";
		}
		?>
	</div>
<br/>
<br/>
	
</div>
</body>
</html>