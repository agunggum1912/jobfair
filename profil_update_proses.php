<?php
	include 'koneksi.php';

	if (isset($_POST['update'])) {
		//tabel users
		$id = $_POST['id']; 
		$nama = $_POST['nama'];
		$no_hp = $_POST['no_hp'];
		//tabel profil
		$id = $_POST ['id'];
		$tempat_lahir = $_POST['tempat_lahir'];
		$tgl_lahir = $_POST['tgl_lahir'];
		$alamat = $_POST['alamat'];
		$kota = $_POST['kota'];
		$provinsi = $_POST['provinsi'];
		$kode_pos = $_POST['kode_pos'];
		$kewarganegaraan = $_POST['kewarganegaraan'];
		$alamat_domisili = $_POST['alamat_domisili'];
		$jenis_kelamin = $_POST['jenis_kelamin'];
		$status_perkawinan = $_POST['status_perkawinan'];
		$agama = $_POST['agama'];

		$jk = ["Laki-laki","Perempuan"];

		if (empty($nama)) {
			echo "<script>alert('Nama harus di isi!');history.go(-1)</script>";
		}elseif(!preg_match("/^[a-zA-Z ]*$/", $nama)){
			echo "<script>alert('Nama hanya boleh menggunakan huruf!');history.go(-1)</script>";
		}elseif (empty($no_hp)) {
			echo "<script>alert('No Handphone harus di isi!');history.go(-1)</script>";	
		}elseif (!preg_match("/^[0-9]*$/", $no_hp)) {
			echo "<script>alert('No Handphone hanya boleh menggunakan Angka!');history.go(-1)</script>";
		}elseif (strlen($no_hp) <= 10 || strlen($no_hp) >= 13) {
			echo "<script>alert('No Handphone hanya boleh 10-13 Angka!');history.go(-1)</script>";
		}elseif (empty($tempat_lahir)) {
			echo "<script>alert('Tempat Lahir harus di isi!');history.go(-1)</script>";
		}elseif (strlen($tempat_lahir) >= 20) {
			echo "<script>alert('Tempat Lahir Maksimal 20 Karakter!');history.go(-1)</script>";
		}elseif (empty($tgl_lahir)) {
			echo "<script>alert('Tanggal Lahir harus di isi!');history.go(-1)</script>";
		}elseif (empty($alamat)) {
			echo "<script>alert('Alamat harus di isi!');history.go(-1)</script>";
		}elseif (strlen($alamat) >= 150) {
			echo "<script>alert('Alamat Maksimal 150 Karakter!');history.go(-1)</script>";
		}elseif (empty($kota)) {
			echo "<script>alert('Kota harus di isi!');history.go(-1)</script>";
		}elseif (strlen($kota) >= 16) {
			echo "<script>alert('Kota Maksimal 16 Karakter!');history.go(-1)</script>";
		}elseif (empty($provinsi)) {
			echo "<script>alert('provinsi harus di isi!');history.go(-1)</script>";
		}elseif (strlen($provinsi) >= 16) {
			echo "<script>alert('Provinsi Maksimal 16 Karakter!');history.go(-1)</script>";
		}elseif (empty($kode_pos)) {
			echo "<script>alert('Kode Pos harus di isi!');history.go(-1)</script>";
		}elseif (!eregi("^[0-9]{5}$", $kode_pos)) {
			echo "<script>alert('Kode Pos yang Anda masukkan salah!');history.go(-1)</script>";
		}elseif (empty($kewarganegaraan)) {
			echo "<script>alert('Kewarganegaraan harus di isi!');history.go(-1)</script>";
		}elseif (strlen($kewarganegaraan) >= 12) {
			echo "<script>alert('Kewarganegaraan Maksimal 12 Angka!');history.go(-1)</script>";
		}elseif (empty($alamat_domisili)) {
			echo "<script>alert('Alamat Domisili harus di isi!');history.go(-1)</script>";
		}elseif (strlen($alamat_domisili) >= 150) {
			echo "<script>alert('Alamat Domisili Maksimal 150 Angka!');history.go(-1)</script>";
		}elseif($jenis_kelamin != "Perempuan" && $jenis_kelamin != "Laki-laki"){
			echo "<script>alert('Jenis Kelamin salah!');history.go(-1)</script>";
		}elseif($status_perkawinan != "Menikah" && $status_perkawinan != "Belum Menikah" && $status_perkawinan != "Cerai Hidup" && $status_perkawinan != "Cerai Mati"){
			echo "<script>alert('Status Perkawinan Salah!');history.go(-1)</script>";
		}elseif($agama != "Islam" && $agama != "Kristen" && $agama != "Katolik" && $agama != "Hindu" && $agama != "Buddha" && $agama != "Kong Hu Cu"){
			echo "<script>alert('Agama Salah!');history.go(-1)</script>";
		}else{
			$query1 = "UPDATE users SET nama='$nama', no_hp='$no_hp' WHERE id='$id'";
			$hasil1 = mysql_query($query1);

			$query2 = "UPDATE profil SET tempat_lahir='$tempat_lahir', tgl_lahir='$tgl_lahir', alamat='$alamat', kota='$kota', provinsi='$provinsi', kode_pos='$kode_pos', kewarganegaraan='$kewarganegaraan', alamat_domisili='$alamat_domisili', jenis_kelamin='$jenis_kelamin', status_perkawinan='$status_perkawinan', agama='$agama' WHERE id='$id'";
			$hasil2 = mysql_query($query2);

		echo "<script>alert('Data telah terupdate.');window.location='home.php'; </script>";
		}
	}
		
	?>