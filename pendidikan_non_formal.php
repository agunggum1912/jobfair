<!DOCTYPE html>
<html>
<head>
	<title>JobFair</title>
	<link rel="stylesheet" type="text/css" href="css/style_home.css">
</head>
<body>

<?php 
include 'koneksi.php';
 
// mengaktifkan session
session_start();
 
// cek apakah user telah login, jika belum login maka di alihkan ke halaman login
if($_SESSION['status'] !="login"){
	header("location:index.php");
}
 
// menampilkan pesan selamat datang
echo "<div class='nav-bar'>";
echo 	"<ul>";
echo 		"<li class='dropdown'><a href='#'>".$_SESSION['email']."</a>";
echo 			"<ul class='isi-dropdown'>";
echo 				"<li><a href='logout.php'>"."Logout"."</a><li>";
echo 				"<li><a href='setting.php'>"."Setting"."</a><li>";
echo 			"</ul>";
echo 		"</li>";
echo 	"</ul>";
echo "</div>";


 
?>

<?php
	$strSQL = "SELECT users.id, users.email, users.nama, profil.id, profil.foto FROM users INNER JOIN profil ON users.id = profil.id WHERE email='$_SESSION[email]' ";
			$query = mysql_query ($strSQL) or die ("query salah");
?>

<div class="left_bar">
	<h3>JobFair</h3>
	<ul>
		<div class="foto_profil">
			<il>
				<?php
					while ($row = mysql_fetch_array($query)) {
						$cek_foto = $row ['foto'];
						$tempat_foto = 'foto/'.$row['foto']; 
						if ($cek_foto >= 1) {
							echo "<a href='setting.php'><img src='$tempat_foto'></a></br>";	
						}else{
							echo "<a href='setting.php'><img src='foto/blank.png'></a></br>";
						}
						echo "<a href='setting.php'>".$row ["nama"]."</a>";
					}
					?>	
			</il>
		</div>
	<il >
		<a class="menu2" href="home.php">My Profile</a>
	</il>
	<li>
		<a class="menu2" href="pendidikan_formal.php">Formal Education</a>
	</li>
	<li>
		<a class="menu" href="pendidikan_non_formal.php">Non Formal Education</a>
	</li>
	<li>
		<a class="menu2" href="pengalaman_kerja.php">Experience</a>
	</li>
	<li>
		<a class="menu2" href="pengalaman_organisasi.php">Organization Activity</a>
	</li>
	<li>
		<a class="menu2" href="keluarga.php">My Family</a>
	</li>
	
</div>
	<div class="hidden">Hidden</div>
<div class="tengah kotak_tengah">
	<div class="kotak_dalam">
		<b>Non 	Formal Education</b>

		<?php

			$strSQL2 = "SELECT users.email, users.id, pendidikan_non_formal.id, pendidikan_non_formal.nama_institusi, pendidikan_non_formal.tipe, pendidikan_non_formal.tgl_mulai, pendidikan_non_formal.tgl_selesai, pendidikan_non_formal.sertifikat, pendidikan_non_formal.nama_institusi2, pendidikan_non_formal.tipe2, pendidikan_non_formal.tgl_mulai2, pendidikan_non_formal.tgl_selesai2, pendidikan_non_formal.sertifikat2, pendidikan_non_formal.nama_institusi3, pendidikan_non_formal.tipe3, pendidikan_non_formal.tgl_mulai3, pendidikan_non_formal.tgl_selesai3, pendidikan_non_formal.sertifikat3, pendidikan_non_formal.nama_institusi4, pendidikan_non_formal.tipe4, pendidikan_non_formal.tgl_mulai4, pendidikan_non_formal.tgl_selesai4, pendidikan_non_formal.sertifikat4, pendidikan_non_formal.nama_institusi5, pendidikan_non_formal.tipe5, pendidikan_non_formal.tgl_mulai5, pendidikan_non_formal.tgl_selesai5, pendidikan_non_formal.sertifikat5 FROM users INNER JOIN pendidikan_non_formal ON users.id = pendidikan_non_formal.id WHERE email='$_SESSION[email]' ";
			$query2 = mysql_query ($strSQL2) or die ("query salah");
			while ($row2 = mysql_fetch_array($query2)){
				$id2 = $row2 ['id'];

			echo "<a class='button_update' href=pendidikan_non_formal_update.php?id=".$id2.">"."Update"."</a>";
			echo "</div>";
			echo "<div>";
			echo 	"<table class='tabel3' border='0'>";
			echo 		"<ul>";
							if ($row2["nama_institusi"]) {
								echo "<tr>";
								echo "<th rowspan='6' scope='col'>";
								echo "</th>";
								echo "<th rowspan='6' scope='col'>";
								echo "</th>";
								echo "</tr>";
								echo "<tr>";
								echo "<th valign='top'>";
								echo "<li>";
								echo 	date("d-m-Y", strtotime($row2 ["tgl_mulai"]))."</br>";
								echo  	date("d-m-Y", strtotime($row2 ["tgl_selesai"]));
								echo "</li>";
								echo "</th>";
								echo "<td align='left' valign='top'>";
								echo  	"<div class='bold'>".$row2 ["tipe"]."</div>";
								echo  	"<div class='bold2'>".$row2 ["nama_institusi"]."</div>";
								echo  	"<div class='italic'>".$row2 ["sertifikat"]."</div>";
								echo "</td>";
								echo "</tr>";
							}else{
								echo "<div class='bold'>Update datamu Sekarang!</div>";
							}
							if ($row2["nama_institusi2"]) {
								echo "<tr>";
								echo "<th valign='top'>";
								echo "<li>";
								echo 	date("d-m-Y", strtotime($row2 ["tgl_mulai2"]))."</br>";
								echo  	date("d-m-Y", strtotime($row2 ["tgl_selesai2"]));
								echo "</li>";
								echo "</th>";
								echo "<td align='left' valign='top'>";
								echo  	"<div class='bold'>".$row2 ["tipe2"]."</div>";
								echo  	"<div class='bold2'>".$row2 ["nama_institusi2"]."</div>";
								echo  	"<div class='italic'>".$row2 ["sertifikat2"]."</div>";
								echo "</td>";
								echo "</tr>";
							}
							if ($row2["nama_institusi3"]) {								
								echo "<tr>";
								echo "<th valign='top'>";
								echo "<li>";
								echo 	date("d-m-Y", strtotime($row2 ["tgl_mulai3"]))."</br>";
								echo  	date("d-m-Y", strtotime($row2 ["tgl_selesai3"]));
								echo "</li>";
								echo "</th>";
								echo "<td align='left' valign='top'>";
								echo  	"<div class='bold'>".$row2 ["tipe3"]."</div>";
								echo  	"<div class='bold2'>".$row2 ["nama_institusi3"]."</div>";
								echo  	"<div class='italic'>".$row2 ["sertifikat3"]."</div>";
								echo "</td>";
								echo "</tr>";
							}
							if ($row2["nama_institusi4"]) {
								echo "<tr>";
								echo "<th valign='top'>";
								echo "<li>";
								echo 	date("d-m-Y", strtotime($row2 ["tgl_mulai4"]))."</br>";
								echo  	date("d-m-Y", strtotime($row2 ["tgl_selesai4"]));
								echo "</li>";
								echo "</th>";
								echo "<td align='left' valign='top'>";
								echo  	"<div class='bold'>".$row2 ["tipe4"]."</div>";
								echo  	"<div class='bold2'>".$row2 ["nama_institusi4"]."</div>";
								echo  	"<div class='italic'>".$row2 ["sertifikat4"]."</div>";
								echo "</td>";
								echo "</tr>";
							}
							if ($row2["nama_institusi5"]) {
								echo "<tr>";
								echo "<th valign='top'>";
								echo "<li>";
								echo 	date("d-m-Y", strtotime($row2 ["tgl_mulai5"]))."</br>";
								echo  	date("d-m-Y", strtotime($row2 ["tgl_selesai5"]));
								echo "</li>";
								echo "</th>";
								echo "<td align='left' valign='top'>";
								echo  	"<div class='bold'>".$row2 ["tipe5"]."</div>";
								echo  	"<div class='bold2'>".$row2 ["nama_institusi5"]."</div>";
								echo  	"<div class='italic'>".$row2 ["sertifikat5"]."</div>";
								echo "</td>";
								echo "</tr>";
							}
			echo 		"</ul>";
			echo 	"</table>";
						}
		?>
	</div>
<br/>
<br/>
	
</div>
</body>
</html>