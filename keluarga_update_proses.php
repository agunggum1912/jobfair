<?php
	include 'koneksi.php';

	if (isset($_POST['update'])) {

		$id = $_POST ['id'];
		$nama_lengkap = $_POST ['nama_lengkap'];
		$hubungan = $_POST ['hubungan'];
		$jenis_kelamin = $_POST ['jenis_kelamin'];
		$pendidikan_terakhir = $_POST ['pendidikan_terakhir'];
		$tempat_lahir = $_POST ['tempat_lahir'];
		$tgl_lahir = $_POST ['tgl_lahir'];
		$pekerjaan = $_POST ['pekerjaan'];
		$nama_lengkap2 = $_POST ['nama_lengkap2'];
		$hubungan2 = $_POST ['hubungan2'];
		$jenis_kelamin2 = $_POST ['jenis_kelamin2'];
		$pendidikan_terakhir2 = $_POST ['pendidikan_terakhir2'];
		$tempat_lahir2 = $_POST ['tempat_lahir2'];
		$tgl_lahir2 = $_POST ['tgl_lahir2'];
		$pekerjaan2 = $_POST ['pekerjaan2'];
		$nama_lengkap3 = $_POST ['nama_lengkap3'];
		$hubungan3 = $_POST ['hubungan3'];
		$jenis_kelamin3 = $_POST ['jenis_kelamin3'];
		$pendidikan_terakhir3 = $_POST ['pendidikan_terakhir3'];
		$tempat_lahir3 = $_POST ['tempat_lahir3'];
		$tgl_lahir3 = $_POST ['tgl_lahir3'];
		$pekerjaan3 = $_POST ['pekerjaan3'];
		$nama_lengkap4 = $_POST ['nama_lengkap4'];
		$hubungan4 = $_POST ['hubungan4'];
		$jenis_kelamin4 = $_POST ['jenis_kelamin4'];
		$pendidikan_terakhir4 = $_POST ['pendidikan_terakhir4'];
		$tempat_lahir4 = $_POST ['tempat_lahir4'];
		$tgl_lahir4 = $_POST ['tgl_lahir4'];
		$pekerjaan4 = $_POST ['pekerjaan4'];
		$nama_lengkap5 = $_POST ['nama_lengkap5'];
		$hubungan5 = $_POST ['hubungan5'];
		$jenis_kelamin5 = $_POST ['jenis_kelamin5'];
		$pendidikan_terakhir5 = $_POST ['pendidikan_terakhir5'];
		$tempat_lahir5 = $_POST ['tempat_lahir5'];
		$tgl_lahir5 = $_POST ['tgl_lahir5'];
		$pekerjaan5 = $_POST ['pekerjaan5'];


		$query = "UPDATE keluarga SET nama_lengkap='$nama_lengkap', hubungan='$hubungan', jenis_kelamin='$jenis_kelamin', pendidikan_terakhir='$pendidikan_terakhir', tempat_lahir='$tempat_lahir', tgl_lahir='$tgl_lahir', pekerjaan='$pekerjaan', nama_lengkap2='$nama_lengkap2', hubungan2='$hubungan2', jenis_kelamin2='$jenis_kelamin2', pendidikan_terakhir2='$pendidikan_terakhir2', tempat_lahir2='$tempat_lahir2', tgl_lahir2='$tgl_lahir2', pekerjaan2='$pekerjaan2', nama_lengkap3='$nama_lengkap3', hubungan3='$hubungan3', jenis_kelamin3='$jenis_kelamin3', pendidikan_terakhir3='$pendidikan_terakhir3', tempat_lahir3='$tempat_lahir3', tgl_lahir3='$tgl_lahir3', pekerjaan3='$pekerjaan3', nama_lengkap4='$nama_lengkap4', hubungan4='$hubungan4', jenis_kelamin4='$jenis_kelamin4', pendidikan_terakhir4='$pendidikan_terakhir4', tempat_lahir4='$tempat_lahir4', tgl_lahir4='$tgl_lahir4', pekerjaan4='$pekerjaan4', nama_lengkap5='$nama_lengkap5', hubungan5='$hubungan5', jenis_kelamin5='$jenis_kelamin5', pendidikan_terakhir5='$pendidikan_terakhir5', tempat_lahir5='$tempat_lahir5', tgl_lahir5='$tgl_lahir5', pekerjaan5='$pekerjaan5' WHERE id='$id'";
		

		if (strlen($nama_lengkap) >= 32) {
			echo "<script>alert('Nama Lengkap pada field pertama Maksimal 32 Karakter!');history.go(-1)</script>";
		}elseif ($nama_lengkap) {
			if (empty($hubungan)) {
				echo "<script>alert('Hubungan pada field pertama harap di isi!');history.go(-1)</script>";
			}elseif ($hubungan != "Ayah" && $hubungan != "Ibu" && $hubungan != "Kakak" && $hubungan != "Adik" && $hubungan != "Suami" && $hubungan != "Istri" && $hubungan != "Anak") {
				echo "<script>alert('Hubungan pada field format salah!');history.go(-1)</script>";
			}elseif (empty($jenis_kelamin)) {
				echo "<script>alert('Jenis Kelamin pada field pertama harap di isi!');history.go(-1)</script>";
			}elseif ($jenis_kelamin != "Laki-laki" && $jenis_kelamin != "Perempuan") {
				echo "<script>alert('Jenis Kemalin pada field pertama format salah!');history.go(-1)</script>";
			}elseif (empty($pendidikan_terakhir)) {
				echo "<script>alert('Pendidikan Terakhir pada field pertama harap di isi!');history.go(-1)</script>";
			}elseif ($pendidikan_terakhir != "SD" && $pendidikan_terakhir != "SMP" && $pendidikan_terakhir != "SLTA" && $pendidikan_terakhir != "Akademik" && $pendidikan_terakhir != "Sarjana") {
				echo "<script>alert('Pendidikan Terakhir pada field pertama format salah!');history.go(-1)</script>";
			}elseif (empty($tempat_lahir)) {
				echo "<script>alert('Tempat Lahir pada field pertama harap di isi!');history.go(-1)</script>";
			}elseif (strlen($tempat_lahir) > 16 ) {
				echo "<script>alert('Tempat Lahir pada field pertama Maksimal 16 Karakter!');history.go(-1)</script>";
			}elseif (empty($tgl_lahir)) {
				echo "<script>alert('Tanggal Lahir pada field pertama harap di isi!');history.go(-1)</script>";
			}elseif (empty($pekerjaan)) {
				echo "<script>alert('Pekerjaan pada field pertama harap di isi!');history.go(-1)</script>";
			}elseif (strlen($pekerjaan) >= 30) {
				echo "<script>alert('Pekerjaan pada field pertama Maksimal 30 Karakter!');history.go(-1)</script>";
			}elseif (strlen($nama_lengkap2) >= 32) {
				echo "<script>alert('Nama Lengkap pada field ke 2 Maksimal 32 Karakter!');history.go(-1)</script>";
			}elseif ($nama_lengkap2) {
				if (empty($hubungan2)) {
					echo "<script>alert('Hubungan pada field ke 2 harap di isi!');history.go(-1)</script>";
				}elseif ($hubungan2 != "Ayah" && $hubungan2 != "Ibu" && $hubungan2 != "Kakak" && $hubungan2 != "Adik" && $hubungan2 != "Suami" && $hubungan2 != "Istri" && $hubungan2 != "Anak") {
					echo "<script>alert('Hubungan pada field Format salah!');history.go(-1)</script>";
				}elseif (empty($jenis_kelamin2)) {
					echo "<script>alert('Jenis Kelamin pada field ke 2 harap di isi!');history.go(-1)</script>";
				}elseif ($jenis_kelamin2 != "Laki-laki" && $jenis_kelamin2 != "Perempuan") {
					echo "<script>alert('Jenis Kemalin pada field ke 2 format salah!');history.go(-1)</script>";
				}elseif (empty($pendidikan_terakhir2)) {
					echo "<script>alert('Pendidikan Terakhir pada field ke 2 harap di isi!');history.go(-1)</script>";
				}elseif ($pendidikan_terakhir2 != "SD" && $pendidikan_terakhir2 != "SMP" && $pendidikan_terakhir2 != "SLTA" && $pendidikan_terakhir2 != "Akademik" && $pendidikan_terakhir2 != "Sarjana") {
					echo "<script>alert('Pendidikan Terakhir pada field ke 2 format salah!');history.go(-1)</script>";
				}elseif (empty($tempat_lahir2)) {
					echo "<script>alert('Tempat Lahir pada field ke 2 harap di isi!');history.go(-1)</script>";
				}elseif (strlen($tempat_lahir2) >= 16) {
					echo "<script>alert('Tempat Lahir pada field ke 2 Maksimal 16 Karakter!');history.go(-1)</script>";
				}elseif (empty($tgl_lahir2)) {
					echo "<script>alert('Tanggal Lahir pada field ke 2 harap di isi!');history.go(-1)</script>";
				}elseif (empty($pekerjaan2)) {
					echo "<script>alert('Pekerjaan pada field ke 2 harap di isi!');history.go(-1)</script>";
				}elseif (strlen($pekerjaan2) >= 30) {
					echo "<script>alert('Pekerjaan pada field ke 2 harap di isi!');history.go(-1)</script>";
				}elseif (strlen($nama_lengkap3) >= 32) {
					echo "<script>alert('Nama Lengkap pada field ke 3 Maksimal 32 Karakter!');history.go(-1)</script>";
				}elseif ($nama_lengkap3) {
					if (empty($hubungan3)) {
						echo "<script>alert('Hubungan pada field ke 3 harap di isi!');history.go(-1)</script>";
					}elseif ($hubungan3 != "Ayah" && $hubungan3 != "Ibu" && $hubungan3 != "Kakak" && $hubungan3 != "Adik" && $hubungan3 != "Suami" && $hubungan3 != "Istri" && $hubungan3 != "Anak") {
						echo "<script>alert('Hubungan pada field ke 3 format salah!');history.go(-1)</script>";
					}elseif (empty($jenis_kelamin3)) {
						echo "<script>alert('Jenis Kelamin pada field ke 3 harap di isi!');history.go(-1)</script>";
					}elseif ($jenis_kelamin3 != "Laki-laki" && $jenis_kelamin3 != "Perempuan") {
						echo "<script>alert('Jenis Kemalin pada field ke 3 format salah!');history.go(-1)</script>";
					}elseif (empty($pendidikan_terakhir3)) {
						echo "<script>alert('Pendidikan Terakhir pada field ke 3 harap di isi!');history.go(-1)</script>";
					}elseif ($pendidikan_terakhir3 != "SD" && $pendidikan_terakhir3 != "SMP" && $pendidikan_terakhir3 != "SLTA" && $pendidikan_terakhir3 != "Akademik" && $pendidikan_terakhir3 != "Sarjana") {
						echo "<script>alert('Pendidikan Terakhir pada field ke 3 format salah!');history.go(-1)</script>";
					}elseif (empty($tempat_lahir3)) {
						echo "<script>alert('Tempat Lahir pada field ke 3 harap di isi!');history.go(-1)</script>";
					}elseif (strlen($tempat_lahir3) >= 16) {
						echo "<script>alert('Tempat Lahir pada field ke 3 Maksimal 16 Karakter!');history.go(-1)</script>";
					}elseif (empty($tgl_lahir3)) {
						echo "<script>alert('Tanggal Lahir pada field ke 3 harap di isi!');history.go(-1)</script>";
					}elseif (empty($pekerjaan3)) {
						echo "<script>alert('Pekerjaan pada field ke 3 harap di isi!');history.go(-1)</script>";
					}elseif (strlen($pekerjaan3) >= 30) {
						echo "<script>alert('Pekerjaan pada field ke 3 harap di isi!');history.go(-1)</script>";
					}elseif (strlen($nama_lengkap4) >= 32) {
						echo "<script>alert('Nama Lengkap pada field ke 4 Maksimal 32 Karakter!');history.go(-1)</script>";
					}elseif ($nama_lengkap4) {
						if (empty($hubungan4)) {
							echo "<script>alert('Hubungan pada field ke 4 harap di isi!');history.go(-1)</script>";
						}elseif ($hubungan4 != "Ayah" && $hubungan4 != "Ibu" && $hubungan4 != "Kakak" && $hubungan4 != "Adik" && $hubungan4 != "Suami" && $hubungan4 != "Istri" && $hubungan4 != "Anak") {
							echo "<script>alert('Hubungan pada field ke 4 format salah!');history.go(-1)</script>";
						}elseif (empty($jenis_kelamin4)) {
							echo "<script>alert('Jenis Kelamin pada field ke 4 harap di isi!');history.go(-1)</script>";
						}elseif ($jenis_kelamin4 != "Laki-laki" && $jenis_kelamin4 != "Perempuan") {
							echo "<script>alert('Jenis Kemalin pada field ke 4 format salah!');history.go(-1)</script>";
						}elseif (empty($pendidikan_terakhir4)) {
							echo "<script>alert('Pendidikan Terakhir pada field ke 4 harap di isi!');history.go(-1)</script>";
						}elseif ($pendidikan_terakhir4 != "SD" && $pendidikan_terakhir4 != "SMP" && $pendidikan_terakhir4 != "SLTA" && $pendidikan_terakhir4 != "Akademik" && $pendidikan_terakhir4 != "Sarjana") {
							echo "<script>alert('Pendidikan Terakhir pada field ke 4 format salah!');history.go(-1)</script>";
						}elseif (empty($tempat_lahir4)) {
							echo "<script>alert('Tempat Lahir pada field ke 4 harap di isi!');history.go(-1)</script>";
						}elseif (strlen($tempat_lahir4) >= 16) {
							echo "<script>alert('Tempat Lahir pada field ke 4 Maksimal 16 Karakter!');history.go(-1)</script>";
						}elseif (empty($tgl_lahir4)) {
							echo "<script>alert('Tanggal Lahir pada field ke 4 harap di isi!');history.go(-1)</script>";
						}elseif (empty($pekerjaan4)) {
							echo "<script>alert('Pekerjaan pada field ke 4 harap di isi!');history.go(-1)</script>";
						}elseif (strlen($pekerjaan4) >= 30) {
							echo "<script>alert('Pekerjaan pada field ke 4 harap di isi!');history.go(-1)</script>";
						}elseif (strlen($nama_lengkap5) >= 32) {
							echo "<script>alert('Nama Lengkap pada field ke 5 Maksimal 32 Karakter!');history.go(-1)</script>";
						}elseif ($nama_lengkap5) {
							if (empty($hubungan5)) {
								echo "<script>alert('Hubungan pada field ke 5 harap di isi!');history.go(-1)</script>";
							}elseif ($hubungan5 != "Ayah" && $hubungan5 != "Ibu" && $hubungan5 != "Kakak" && $hubungan5 != "Adik" && $hubungan5 != "Suami" && $hubungan5 != "Istri" && $hubungan5 != "Anak") {
								echo "<script>alert('Hubungan pada field ke 5 format salah!');history.go(-1)</script>";
							}elseif (empty($jenis_kelamin5)) {
								echo "<script>alert('Jenis Kelamin pada field ke 5 harap di isi!');history.go(-1)</script>";
							}elseif ($jenis_kelamin5 != "Laki-laki" && $jenis_kelamin5 != "Perempuan") {
								echo "<script>alert('Jenis Kemalin pada field ke 5 format salah!');history.go(-1)</script>";
							}elseif (empty($pendidikan_terakhir5)) {
								echo "<script>alert('Pendidikan Terakhir pada field ke 5 harap di isi!');history.go(-1)</script>";
							}elseif ($pendidikan_terakhir5 != "SD" && $pendidikan_terakhir5 != "SMP" && $pendidikan_terakhir5 != "SLTA" && $pendidikan_terakhir5 != "Akademik" && $pendidikan_terakhir5 != "Sarjana") {
								echo "<script>alert('Pendidikan Terakhir pada field ke 5 format salah!');history.go(-1)</script>";
							}elseif (empty($tempat_lahir5)) {
								echo "<script>alert('Tempat Lahir pada field ke 5 harap di isi!');history.go(-1)</script>";
							}elseif (strlen($tempat_lahir5) >= 16) {
								echo "<script>alert('Tempat Lahir pada field ke 5 Maksimal 16 Karakter!');history.go(-1)</script>";
							}elseif (empty($tgl_lahir5)) {
								echo "<script>alert('Tanggal Lahir pada field ke 5 harap di isi!');history.go(-1)</script>";
							}elseif (empty($pekerjaan5)) {
								echo "<script>alert('Pekerjaan pada field ke 5 harap di isi!');history.go(-1)</script>";
							}elseif (strlen($pekerjaan5) >= 30) {
								echo "<script>alert('Pekerjaan pada field ke 5 harap di isi!');history.go(-1)</script>";
							}else{
								$hasil = mysql_query($query);
								echo "<script>alert('Data telah terupdate.');window.location='keluarga.php'; </script>";
							}
						}else{
							$hasil = mysql_query($query);
							echo "<script>alert('Data telah terupdate.');window.location='keluarga.php'; </script>";
						}
					}else{
						$hasil = mysql_query($query);
						echo "<script>alert('Data telah terupdate.');window.location='keluarga.php'; </script>";
					}
				}else{
					$hasil = mysql_query($query);
					echo "<script>alert('Data telah terupdate.');window.location='keluarga.php'; </script>";
				}
			}else{
				$hasil = mysql_query($query);
				echo "<script>alert('Data telah terupdate.');window.location='keluarga.php'; </script>";
			}
		}
	}
		
	?>