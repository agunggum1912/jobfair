<?php
	include 'koneksi.php';

	if (isset($_POST['update'])) {

		$id = $_POST ['id'];
		$nama_organisasi = $_POST ['nama_organisasi'];
		$kota = $_POST ['kota'];
		$bidang_organisasi = $_POST ['bidang_organisasi'];
		$lama_bergabung = $_POST ['lama_bergabung'];
		$posisi = $_POST ['posisi'];
		$nama_organisasi2 = $_POST ['nama_organisasi2'];
		$kota2 = $_POST ['kota2'];
		$bidang_organisasi2 = $_POST ['bidang_organisasi2'];
		$lama_bergabung2 = $_POST ['lama_bergabung2'];
		$posisi2 = $_POST ['posisi2'];
		$nama_organisasi3 = $_POST ['nama_organisasi3'];
		$kota3 = $_POST ['kota3'];
		$bidang_organisasi3 = $_POST ['bidang_organisasi3'];
		$lama_bergabung3 = $_POST ['lama_bergabung3'];
		$posisi3 = $_POST ['posisi3'];
		$nama_organisasi4 = $_POST ['nama_organisasi4'];
		$kota4 = $_POST ['kota4'];
		$bidang_organisasi4 = $_POST ['bidang_organisasi4'];
		$lama_bergabung4 = $_POST ['lama_bergabung4'];
		$posisi4 = $_POST ['posisi4'];
		$nama_organisasi5 = $_POST ['nama_organisasi5'];
		$kota5 = $_POST ['kota5'];
		$bidang_organisasi5 = $_POST ['bidang_organisasi5'];
		$lama_bergabung5 = $_POST ['lama_bergabung5'];
		$posisi5 = $_POST ['posisi5'];


		$query = "UPDATE pengalaman_organisasi SET nama_organisasi='$nama_organisasi', kota='$kota', bidang_organisasi='$bidang_organisasi', lama_bergabung='$lama_bergabung', posisi='$posisi', nama_organisasi2='$nama_organisasi2', kota2='$kota2', bidang_organisasi2='$bidang_organisasi2', lama_bergabung2='$lama_bergabung2', posisi2='$posisi2', nama_organisasi3='$nama_organisasi3', kota3='$kota3', bidang_organisasi3='$bidang_organisasi3', lama_bergabung3='$lama_bergabung3', posisi3='$posisi3', nama_organisasi4='$nama_organisasi4', kota4='$kota4', bidang_organisasi4='$bidang_organisasi4', lama_bergabung4='$lama_bergabung4', posisi4='$posisi4', nama_organisasi5='$nama_organisasi5', kota5='$kota5', bidang_organisasi5='$bidang_organisasi5', lama_bergabung5='$lama_bergabung5', posisi5='$posisi5' WHERE id='$id'";
		

		if (strlen($nama_organisasi) >= 30) {
			echo "<script>alert('Nama Organisasi pada field Pertama Maksimal 30 Karakter!');history.go(-1)</script>";
		}elseif ($nama_organisasi) {
			if (empty($kota)) {
				echo "<script>alert('Kota pada field Pertama harap di isi!');history.go(-1)</script>";
			}elseif (strlen($kota) >= 16) {
				echo "<script>alert('Kota pada field Pertama Maksimal 16 Karakter!');history.go(-1)</script>";
			}elseif (empty($bidang_organisasi)) {
				echo "<script>alert('Bidang Organisasi pada field Pertama harap di isi!');history.go(-1)</script>";
			}elseif (strlen($bidang_organisasi) >= 36) {
				echo "<script>alert('Bidang Organisasi pada field Pertama Maksimal 36 Karakter!');history.go(-1)</script>";
			}elseif (empty($lama_bergabung)) {
				echo "<script>alert('Lama Bergabung pada field Pertama harap di isi!');history.go(-1)</script>";
			}elseif (!eregi("^[0-9]{1,3}$", $lama_bergabung)) {
				echo "<script>alert('Lama Bergabung pada field Pertama format salah!');history.go(-1)</script>";
			}elseif (empty($posisi)) {
				echo "<script>alert('Posisi pada field Pertama harap di isi!');history.go(-1)</script>";
			}elseif (strlen($posisi) >= 16) {
				echo "<script>alert('Posisi pada field Pertama Maksimal 16 Karakter!');history.go(-1)</script>";
			}elseif (strlen($nama_organisasi2) >= 30) {
				echo "<script>alert('Nama Organisasi pada field ke 2 Maksimal 30 Karakter!');history.go(-1)</script>";
			}elseif ($nama_organisasi2) {
				if (empty($kota2)) {
				echo "<script>alert('Kota pada field ke 2 harap di isi!');history.go(-1)</script>";
				}elseif (strlen($kota2) >= 16) {
					echo "<script>alert('Kota pada field ke 2 Maksimal 16 Karakter!');history.go(-1)</script>";
				}elseif (empty($bidang_organisasi2)) {
					echo "<script>alert('Bidang Organisasi pada field ke 2 harap di isi!');history.go(-1)</script>";
				}elseif (strlen($bidang_organisasi2) >= 36) {
					echo "<script>alert('Bidang Organisasi pada field ke 2 Maksimal 36 Karakter!');history.go(-1)</script>";
				}elseif (empty($lama_bergabung2)) {
					echo "<script>alert('Lama Bergabung pada field ke 2 harap di isi!');history.go(-1)</script>";
				}elseif (!eregi("^[0-9]{1,3}$", $lama_bergabung2)) {
					echo "<script>alert('Lama Bergabung pada field ke 2 format salah!');history.go(-1)</script>";
				}elseif (empty($posisi2)) {
					echo "<script>alert('Posisi pada field ke 2 harap di isi!');history.go(-1)</script>";
				}elseif (strlen($posisi2) >= 16) {
					echo "<script>alert('Posisi pada field ke 2 Maksimal 16 Karakter!');history.go(-1)</script>";
				}elseif (strlen($nama_organisasi3) >= 30) {
					echo "<script>alert('Nama Organisasi pada field ke 3 Maksimal 30 Karakter!');history.go(-1)</script>";
				}elseif ($nama_organisasi3) {
					if (empty($kota3)) {
					echo "<script>alert('Kota pada field ke 3 harap di isi!');history.go(-1)</script>";
					}elseif (strlen($kota3) >= 16) {
						echo "<script>alert('Kota pada field ke 3 Maksimal 16 Karakter!');history.go(-1)</script>";
					}elseif (empty($bidang_organisasi3)) {
						echo "<script>alert('Bidang Organisasi pada field ke 3 harap di isi!');history.go(-1)</script>";
					}elseif (strlen($bidang_organisasi3) >= 36) {
						echo "<script>alert('Bidang Organisasi pada field ke 3 Maksimal 36 Karakter!');history.go(-1)</script>";
					}elseif (empty($lama_bergabung3)) {
						echo "<script>alert('Lama Bergabung pada field ke 3 harap di isi!');history.go(-1)</script>";
					}elseif (!eregi("^[0-9]{1,3}$", $lama_bergabung3)) {
						echo "<script>alert('Lama Bergabung pada field ke 3 format salah!');history.go(-1)</script>";
					}elseif (empty($posisi3)) {
						echo "<script>alert('Posisi pada field ke 3 harap di isi!');history.go(-1)</script>";
					}elseif (strlen($posisi3) >= 16) {
						echo "<script>alert('Posisi pada field ke 3 Maksimal 16 Karakter!');history.go(-1)</script>";
					}elseif (strlen($nama_organisasi4) >= 30) {
						echo "<script>alert('Nama Organisasi pada field ke 3 Maksimal 30 Karakter!');history.go(-1)</script>";
					}elseif ($nama_organisasi4) {
						if (empty($kota4)) {
						echo "<script>alert('Kota pada field ke 4 harap di isi!');history.go(-1)</script>";
						}elseif (strlen($kota4) >= 16) {
							echo "<script>alert('Kota pada field ke 4 Maksimal 16 Karakter!');history.go(-1)</script>";
						}elseif (empty($bidang_organisasi4)) {
							echo "<script>alert('Bidang Organisasi pada field ke 4 harap di isi!');history.go(-1)</script>";
						}elseif (strlen($bidang_organisasi4) >= 36) {
							echo "<script>alert('Bidang Organisasi pada field ke 4 Maksimal 36 Karakter!');history.go(-1)</script>";
						}elseif (empty($lama_bergabung4)) {
							echo "<script>alert('Lama Bergabung pada field ke 4 harap di isi!');history.go(-1)</script>";
						}elseif (!eregi("^[0-9]{1,3}$", $lama_bergabung4)) {
							echo "<script>alert('Lama Bergabung pada field ke 4 format salah!');history.go(-1)</script>";
						}elseif (empty($posisi4)) {
							echo "<script>alert('Posisi pada field ke 4 harap di isi!');history.go(-1)</script>";
						}elseif (strlen($posisi4) >= 16) {
							echo "<script>alert('Posisi pada field ke 4 Maksimal 16 Karakter!');history.go(-1)</script>";
						}elseif (strlen($nama_organisasi5) >= 30) {
							echo "<script>alert('Nama Organisasi pada field ke 5 Maksimal 30 Karakter!');history.go(-1)</script>";
						}elseif ($nama_organisasi5) {
							if (empty($kota5)) {
							echo "<script>alert('Kota pada field ke 5 harap di isi!');history.go(-1)</script>";
							}elseif (strlen($kota5) >= 16) {
								echo "<script>alert('Kota pada field ke 5 Maksimal 16 Karakter!');history.go(-1)</script>";
							}elseif (empty($bidang_organisasi5)) {
								echo "<script>alert('Bidang Organisasi pada field ke 5 harap di isi!');history.go(-1)</script>";
							}elseif (strlen($bidang_organisasi5) >= 36) {
								echo "<script>alert('Bidang Organisasi pada field ke 5 Maksimal 36 Karakter!');history.go(-1)</script>";
							}elseif (empty($lama_bergabung5)) {
								echo "<script>alert('Lama Bergabung pada field ke 5 harap di isi!');history.go(-1)</script>";
							}elseif (!eregi("^[0-9]{1,3}$", $lama_bergabung5)) {
								echo "<script>alert('Lama Bergabung pada field ke 5 format salah!');history.go(-1)</script>";
							}elseif (empty($posisi5)) {
								echo "<script>alert('Posisi pada field ke 5 harap di isi!');history.go(-1)</script>";
							}elseif (strlen($posisi5) >= 16) {
								echo "<script>alert('Posisi pada field ke 5 Maksimal 16 Karakter!');history.go(-1)</script>";
							}else{
								$hasil = mysql_query($query);
								echo "<script>alert('Data telah terupdate.');window.location='pengalaman_organisasi.php'; </script>";
							}
						}else{
							$hasil = mysql_query($query);
							echo "<script>alert('Data telah terupdate.');window.location='pengalaman_organisasi.php'; </script>";
						}
					}else{
						$hasil = mysql_query($query);
						echo "<script>alert('Data telah terupdate.');window.location='pengalaman_organisasi.php'; </script>";
					}
				}else{
					$hasil = mysql_query($query);
					echo "<script>alert('Data telah terupdate.');window.location='pengalaman_organisasi.php'; </script>";
				}
			}else{
				$hasil = mysql_query($query);
				echo "<script>alert('Data telah terupdate.');window.location='pengalaman_organisasi.php'; </script>";
			}
		}
	}
		
	?>