-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 03 Feb 2020 pada 09.20
-- Versi server: 10.1.32-MariaDB
-- Versi PHP: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jobfair_db`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `keluarga`
--

CREATE TABLE `keluarga` (
  `id` int(10) NOT NULL,
  `nama_lengkap` varchar(32) NOT NULL,
  `hubungan` varchar(24) NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `pendidikan_terakhir` varchar(12) NOT NULL,
  `tempat_lahir` varchar(16) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `pekerjaan` varchar(30) NOT NULL,
  `nama_lengkap2` varchar(32) NOT NULL,
  `hubungan2` varchar(24) NOT NULL,
  `jenis_kelamin2` varchar(10) NOT NULL,
  `pendidikan_terakhir2` varchar(12) NOT NULL,
  `tempat_lahir2` varchar(16) NOT NULL,
  `tgl_lahir2` date NOT NULL,
  `pekerjaan2` varchar(30) NOT NULL,
  `nama_lengkap3` varchar(32) NOT NULL,
  `hubungan3` varchar(24) NOT NULL,
  `jenis_kelamin3` varchar(10) NOT NULL,
  `pendidikan_terakhir3` varchar(12) NOT NULL,
  `tempat_lahir3` varchar(16) NOT NULL,
  `tgl_lahir3` date NOT NULL,
  `pekerjaan3` varchar(30) NOT NULL,
  `nama_lengkap4` varchar(32) NOT NULL,
  `hubungan4` varchar(24) NOT NULL,
  `jenis_kelamin4` varchar(10) NOT NULL,
  `pendidikan_terakhir4` varchar(12) NOT NULL,
  `tempat_lahir4` varchar(16) NOT NULL,
  `tgl_lahir4` date NOT NULL,
  `pekerjaan4` varchar(30) NOT NULL,
  `nama_lengkap5` varchar(32) NOT NULL,
  `hubungan5` varchar(24) NOT NULL,
  `jenis_kelamin5` varchar(10) NOT NULL,
  `pendidikan_terakhir5` varchar(12) NOT NULL,
  `tempat_lahir5` varchar(16) NOT NULL,
  `tgl_lahir5` date NOT NULL,
  `pekerjaan5` varchar(30) NOT NULL,
  `nama_lengkap6` varchar(30) NOT NULL,
  `hubungan6` varchar(24) NOT NULL,
  `jenis_kelamin6` varchar(10) NOT NULL,
  `pendidikan_terakhir6` varchar(12) NOT NULL,
  `tempat_lahir6` varchar(16) NOT NULL,
  `tgl_lahir6` date NOT NULL,
  `pekerjaan6` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `keluarga`
--

INSERT INTO `keluarga` (`id`, `nama_lengkap`, `hubungan`, `jenis_kelamin`, `pendidikan_terakhir`, `tempat_lahir`, `tgl_lahir`, `pekerjaan`, `nama_lengkap2`, `hubungan2`, `jenis_kelamin2`, `pendidikan_terakhir2`, `tempat_lahir2`, `tgl_lahir2`, `pekerjaan2`, `nama_lengkap3`, `hubungan3`, `jenis_kelamin3`, `pendidikan_terakhir3`, `tempat_lahir3`, `tgl_lahir3`, `pekerjaan3`, `nama_lengkap4`, `hubungan4`, `jenis_kelamin4`, `pendidikan_terakhir4`, `tempat_lahir4`, `tgl_lahir4`, `pekerjaan4`, `nama_lengkap5`, `hubungan5`, `jenis_kelamin5`, `pendidikan_terakhir5`, `tempat_lahir5`, `tgl_lahir5`, `pekerjaan5`, `nama_lengkap6`, `hubungan6`, `jenis_kelamin6`, `pendidikan_terakhir6`, `tempat_lahir6`, `tgl_lahir6`, `pekerjaan6`) VALUES
(41, 'Green Nursyekha', 'Ayah', 'Laki-laki', 'SLTA', 'Margasari', '1975-05-15', 'Wirausaha', 'Bella Pearce', 'Ibu', 'Perempuan', 'SLTA', 'Tigaraksa', '1978-08-18', 'Wirausaha', 'Novia Syahnaz', 'Kakak', 'Perempuan', 'Sarjana', 'Tangerang', '1999-12-31', 'Karyawan Swasta', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '0000-00-00', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendidikan_formal`
--

CREATE TABLE `pendidikan_formal` (
  `id` int(10) NOT NULL,
  `nama_institusi` varchar(30) NOT NULL,
  `jurusan` varchar(36) NOT NULL,
  `kota` varchar(16) NOT NULL,
  `tahun_mulai` int(4) NOT NULL,
  `tahun_selesai` int(4) NOT NULL,
  `nama_institusi2` varchar(30) NOT NULL,
  `jurusan2` varchar(36) NOT NULL,
  `kota2` varchar(16) NOT NULL,
  `tahun_mulai2` int(4) NOT NULL,
  `tahun_selesai2` int(4) NOT NULL,
  `nama_institusi3` varchar(30) NOT NULL,
  `jurusan3` varchar(36) NOT NULL,
  `kota3` varchar(16) NOT NULL,
  `tahun_mulai3` int(4) NOT NULL,
  `tahun_selesai3` int(4) NOT NULL,
  `nama_institusi4` varchar(30) NOT NULL,
  `jurusan4` varchar(36) NOT NULL,
  `kota4` varchar(16) NOT NULL,
  `tahun_mulai4` int(4) NOT NULL,
  `tahun_selesai4` int(4) NOT NULL,
  `nama_institusi5` varchar(30) NOT NULL,
  `jurusan5` varchar(36) NOT NULL,
  `kota5` varchar(16) NOT NULL,
  `tahun_mulai5` int(4) NOT NULL,
  `tahun_selesai5` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pendidikan_formal`
--

INSERT INTO `pendidikan_formal` (`id`, `nama_institusi`, `jurusan`, `kota`, `tahun_mulai`, `tahun_selesai`, `nama_institusi2`, `jurusan2`, `kota2`, `tahun_mulai2`, `tahun_selesai2`, `nama_institusi3`, `jurusan3`, `kota3`, `tahun_mulai3`, `tahun_selesai3`, `nama_institusi4`, `jurusan4`, `kota4`, `tahun_mulai4`, `tahun_selesai4`, `nama_institusi5`, `jurusan5`, `kota5`, `tahun_mulai5`, `tahun_selesai5`) VALUES
(41, 'SD N Green', '', 'Tangerang', 2008, 2014, 'SMP N 1 Save Earth', '', 'Tangerang', 2014, 2017, 'SMK N 1 Save World', '', 'Tangerang', 2017, 2020, '', '', '', 0, 0, '', '', '', 0, 0);

--
-- Trigger `pendidikan_formal`
--
DELIMITER $$
CREATE TRIGGER `after_insert_pendidikan_formal` AFTER INSERT ON `pendidikan_formal` FOR EACH ROW BEGIN
	INSERT INTO keluarga (id) VALUES (NEW.id);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendidikan_non_formal`
--

CREATE TABLE `pendidikan_non_formal` (
  `id` int(10) NOT NULL,
  `nama_institusi` varchar(30) NOT NULL,
  `tipe` varchar(12) NOT NULL,
  `tgl_mulai` date NOT NULL,
  `tgl_selesai` date NOT NULL,
  `sertifikat` varchar(19) NOT NULL,
  `nama_institusi2` varchar(30) NOT NULL,
  `tipe2` varchar(12) NOT NULL,
  `tgl_mulai2` date NOT NULL,
  `tgl_selesai2` date NOT NULL,
  `sertifikat2` varchar(19) NOT NULL,
  `nama_institusi3` varchar(30) NOT NULL,
  `tipe3` varchar(12) NOT NULL,
  `tgl_mulai3` date NOT NULL,
  `tgl_selesai3` date NOT NULL,
  `sertifikat3` varchar(19) NOT NULL,
  `nama_institusi4` varchar(30) NOT NULL,
  `tipe4` varchar(12) NOT NULL,
  `tgl_mulai4` date NOT NULL,
  `tgl_selesai4` date NOT NULL,
  `sertifikat4` varchar(19) NOT NULL,
  `nama_institusi5` varchar(30) NOT NULL,
  `tipe5` varchar(12) NOT NULL,
  `tgl_mulai5` date NOT NULL,
  `tgl_selesai5` date NOT NULL,
  `sertifikat5` varchar(19) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pendidikan_non_formal`
--

INSERT INTO `pendidikan_non_formal` (`id`, `nama_institusi`, `tipe`, `tgl_mulai`, `tgl_selesai`, `sertifikat`, `nama_institusi2`, `tipe2`, `tgl_mulai2`, `tgl_selesai2`, `sertifikat2`, `nama_institusi3`, `tipe3`, `tgl_mulai3`, `tgl_selesai3`, `sertifikat3`, `nama_institusi4`, `tipe4`, `tgl_mulai4`, `tgl_selesai4`, `sertifikat4`, `nama_institusi5`, `tipe5`, `tgl_mulai5`, `tgl_selesai5`, `sertifikat5`) VALUES
(41, 'Gerakan Pernghijauan Dunia', 'WORKSHOP', '2015-05-15', '2016-06-16', 'Bersertifikat', '', '', '0000-00-00', '0000-00-00', '', '', '', '0000-00-00', '0000-00-00', '', '', '', '0000-00-00', '0000-00-00', '', '', '', '0000-00-00', '0000-00-00', '');

--
-- Trigger `pendidikan_non_formal`
--
DELIMITER $$
CREATE TRIGGER `after_insert_pendidikan_non_formal` AFTER INSERT ON `pendidikan_non_formal` FOR EACH ROW BEGIN
	INSERT INTO pendidikan_formal (id) VALUES (NEW.id);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengalaman_kerja`
--

CREATE TABLE `pengalaman_kerja` (
  `id` int(10) NOT NULL,
  `nama_perusahaan` varchar(30) NOT NULL,
  `alamat_perusahaan` varchar(72) NOT NULL,
  `tgl_mulai` date NOT NULL,
  `tgl_selesai` date NOT NULL,
  `posisi_terakhir` varchar(36) NOT NULL,
  `gaji` int(10) NOT NULL,
  `job_desk` varchar(200) NOT NULL,
  `nama_perusahaan2` varchar(30) NOT NULL,
  `alamat_perusahaan2` varchar(72) NOT NULL,
  `tgl_mulai2` date NOT NULL,
  `tgl_selesai2` date NOT NULL,
  `posisi_terakhir2` varchar(36) NOT NULL,
  `gaji2` int(10) NOT NULL,
  `job_desk2` varchar(200) NOT NULL,
  `nama_perusahaan3` varchar(30) NOT NULL,
  `alamat_perusahaan3` varchar(72) NOT NULL,
  `tgl_mulai3` date NOT NULL,
  `tgl_selesai3` date NOT NULL,
  `posisi_terakhir3` varchar(36) NOT NULL,
  `gaji3` int(10) NOT NULL,
  `job_desk3` varchar(200) NOT NULL,
  `nama_perusahaan4` varchar(30) NOT NULL,
  `alamat_perusahaan4` varchar(72) NOT NULL,
  `tgl_mulai4` date NOT NULL,
  `tgl_selesai4` date NOT NULL,
  `posisi_terakhir4` varchar(36) NOT NULL,
  `gaji4` int(10) NOT NULL,
  `job_desk4` varchar(200) NOT NULL,
  `nama_perusahaan5` varchar(30) NOT NULL,
  `alamat_perusahaan5` varchar(72) NOT NULL,
  `tgl_mulai5` date NOT NULL,
  `tgl_selesai5` date NOT NULL,
  `posisi_terakhir5` varchar(36) NOT NULL,
  `gaji5` int(10) NOT NULL,
  `job_desk5` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengalaman_kerja`
--

INSERT INTO `pengalaman_kerja` (`id`, `nama_perusahaan`, `alamat_perusahaan`, `tgl_mulai`, `tgl_selesai`, `posisi_terakhir`, `gaji`, `job_desk`, `nama_perusahaan2`, `alamat_perusahaan2`, `tgl_mulai2`, `tgl_selesai2`, `posisi_terakhir2`, `gaji2`, `job_desk2`, `nama_perusahaan3`, `alamat_perusahaan3`, `tgl_mulai3`, `tgl_selesai3`, `posisi_terakhir3`, `gaji3`, `job_desk3`, `nama_perusahaan4`, `alamat_perusahaan4`, `tgl_mulai4`, `tgl_selesai4`, `posisi_terakhir4`, `gaji4`, `job_desk4`, `nama_perusahaan5`, `alamat_perusahaan5`, `tgl_mulai5`, `tgl_selesai5`, `posisi_terakhir5`, `gaji5`, `job_desk5`) VALUES
(41, 'Kementrian Lingkungan Hidup', 'Jl Ayo Hijau No 1, Jakarta', '2019-09-19', '2020-01-01', 'Magang', 6000000, 'Melakukan penghijauan terhadap lingkungan di masyarakat sekitar', '', '', '0000-00-00', '0000-00-00', '', 0, '', '', '', '0000-00-00', '0000-00-00', '', 0, '', '', '', '0000-00-00', '0000-00-00', '', 0, '', '', '', '0000-00-00', '0000-00-00', '', 0, '');

--
-- Trigger `pengalaman_kerja`
--
DELIMITER $$
CREATE TRIGGER `after_insert_pengalaman_kerja` AFTER INSERT ON `pengalaman_kerja` FOR EACH ROW BEGIN
	INSERT INTO pendidikan_non_formal (id) VALUES (NEW.id);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengalaman_organisasi`
--

CREATE TABLE `pengalaman_organisasi` (
  `id` int(10) NOT NULL,
  `nama_organisasi` varchar(30) NOT NULL,
  `kota` varchar(16) NOT NULL,
  `bidang_organisasi` varchar(36) NOT NULL,
  `lama_bergabung` int(3) NOT NULL,
  `posisi` varchar(16) NOT NULL,
  `nama_organisasi2` varchar(30) NOT NULL,
  `kota2` varchar(16) NOT NULL,
  `bidang_organisasi2` varchar(36) NOT NULL,
  `lama_bergabung2` int(3) NOT NULL,
  `posisi2` varchar(16) NOT NULL,
  `nama_organisasi3` varchar(30) NOT NULL,
  `kota3` varchar(16) NOT NULL,
  `bidang_organisasi3` varchar(36) NOT NULL,
  `lama_bergabung3` int(3) NOT NULL,
  `posisi3` varchar(16) NOT NULL,
  `nama_organisasi4` varchar(30) NOT NULL,
  `kota4` varchar(16) NOT NULL,
  `bidang_organisasi4` varchar(36) NOT NULL,
  `lama_bergabung4` int(3) NOT NULL,
  `posisi4` varchar(16) NOT NULL,
  `nama_organisasi5` varchar(30) NOT NULL,
  `kota5` varchar(16) NOT NULL,
  `bidang_organisasi5` varchar(36) NOT NULL,
  `lama_bergabung5` int(3) NOT NULL,
  `posisi5` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengalaman_organisasi`
--

INSERT INTO `pengalaman_organisasi` (`id`, `nama_organisasi`, `kota`, `bidang_organisasi`, `lama_bergabung`, `posisi`, `nama_organisasi2`, `kota2`, `bidang_organisasi2`, `lama_bergabung2`, `posisi2`, `nama_organisasi3`, `kota3`, `bidang_organisasi3`, `lama_bergabung3`, `posisi3`, `nama_organisasi4`, `kota4`, `bidang_organisasi4`, `lama_bergabung4`, `posisi4`, `nama_organisasi5`, `kota5`, `bidang_organisasi5`, `lama_bergabung5`, `posisi5`) VALUES
(41, 'Sekolah Pecinta Alam', 'Tangerang', 'Gerakan Kemasyarakatan', 36, 'Ketua', '', '', '', 0, '', '', '', '', 0, '', '', '', '', 0, '', '', '', '', 0, '');

--
-- Trigger `pengalaman_organisasi`
--
DELIMITER $$
CREATE TRIGGER `after_insert_pengalaman_organisasi` AFTER INSERT ON `pengalaman_organisasi` FOR EACH ROW BEGIN
	INSERT INTO pengalaman_kerja (id) VALUES (NEW.id);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `profil`
--

CREATE TABLE `profil` (
  `id` int(10) NOT NULL,
  `tempat_lahir` varchar(20) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenis_kelamin` varchar(9) NOT NULL,
  `alamat` varchar(150) NOT NULL,
  `provinsi` varchar(16) NOT NULL,
  `kota` varchar(16) NOT NULL,
  `kecamatan` varchar(16) NOT NULL,
  `kode_pos` int(5) NOT NULL,
  `alamat_domisili` varchar(150) NOT NULL,
  `kewarganegaraan` varchar(12) NOT NULL,
  `agama` varchar(10) NOT NULL,
  `status_perkawinan` varchar(16) NOT NULL,
  `foto` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `profil`
--

INSERT INTO `profil` (`id`, `tempat_lahir`, `tgl_lahir`, `jenis_kelamin`, `alamat`, `provinsi`, `kota`, `kecamatan`, `kode_pos`, `alamat_domisili`, `kewarganegaraan`, `agama`, `status_perkawinan`, `foto`) VALUES
(41, 'Margasari', '2002-02-02', 'Laki-laki', 'Kp Dunia Sehat Rt 003 Rw 003, Desa Margasari, Kec. Tigaraksa', 'Banten', '15720', '', 15720, 'Kp Dunia Sehat Rt 003 Rw 003, Desa Margasari, Kec. Tigaraksa', 'Indonesia', 'Islam', 'Menikah', '2020020308595920200202061136Save Earth.jpg');

--
-- Trigger `profil`
--
DELIMITER $$
CREATE TRIGGER `after_insert_profil` AFTER INSERT ON `profil` FOR EACH ROW BEGIN
	INSERT INTO pengalaman_organisasi (id) VALUES (NEW.id);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `email` varchar(36) NOT NULL,
  `nama` varchar(32) NOT NULL,
  `password` varchar(100) NOT NULL,
  `no_hp` varchar(14) NOT NULL,
  `tgl_daftar` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `email`, `nama`, `password`, `no_hp`, `tgl_daftar`) VALUES
(41, 'admin@gmail.com', 'Admin', 'c6cd6d0f22ec0f68da2296231ba6f133', '08777888999', '2020-02-03 08:58:53');

--
-- Trigger `users`
--
DELIMITER $$
CREATE TRIGGER `after_insert_user` AFTER INSERT ON `users` FOR EACH ROW BEGIN
	INSERT INTO profil (id) VALUES (NEW.id);
END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `keluarga`
--
ALTER TABLE `keluarga`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pendidikan_formal`
--
ALTER TABLE `pendidikan_formal`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pendidikan_non_formal`
--
ALTER TABLE `pendidikan_non_formal`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pengalaman_kerja`
--
ALTER TABLE `pengalaman_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pengalaman_organisasi`
--
ALTER TABLE `pengalaman_organisasi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `profil`
--
ALTER TABLE `profil`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `keluarga`
--
ALTER TABLE `keluarga`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `pendidikan_formal`
--
ALTER TABLE `pendidikan_formal`
  ADD CONSTRAINT `pendidikan_formal_ibfk_1` FOREIGN KEY (`id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
