<?php
	include 'koneksi.php';

	if (isset($_POST['update'])) {

		$id = $_POST['id']; 
		$nama_institusi = $_POST['nama_institusi'];
		$kota = $_POST['kota'];
		$jurusan = $_POST['jurusan'];
		$tahun_mulai = $_POST['tahun_mulai'];
		$tahun_selesai = $_POST['tahun_selesai'];
		$nama_institusi2 = $_POST['nama_institusi2'];
		$kota2 = $_POST['kota2'];
		$jurusan2 = $_POST['jurusan2'];
		$tahun_mulai2 = $_POST['tahun_mulai2'];
		$tahun_selesai2 = $_POST['tahun_selesai2'];
		$nama_institusi3 = $_POST['nama_institusi3'];
		$kota3 = $_POST['kota3'];
		$jurusan3 = $_POST['jurusan3'];
		$tahun_mulai3 = $_POST['tahun_mulai3'];
		$tahun_selesai3 = $_POST['tahun_selesai3'];
		$nama_institusi4 = $_POST['nama_institusi4'];
		$kota4 = $_POST['kota4'];
		$jurusan4 = $_POST['jurusan4'];
		$tahun_mulai4 = $_POST['tahun_mulai4'];
		$tahun_selesai4 = $_POST['tahun_selesai4'];
		$nama_institusi5 = $_POST['nama_institusi5'];
		$kota5 = $_POST['kota5'];
		$jurusan5 = $_POST['jurusan5'];
		$tahun_mulai5 = $_POST['tahun_mulai5'];
		$tahun_selesai5 = $_POST['tahun_selesai5'];

		$query = "UPDATE pendidikan_formal SET nama_institusi='$nama_institusi', jurusan='$jurusan', kota='$kota', tahun_mulai='$tahun_mulai', tahun_selesai='$tahun_selesai', nama_institusi2='$nama_institusi2', jurusan2='$jurusan2', kota2='$kota2', tahun_mulai2='$tahun_mulai2', tahun_selesai2='$tahun_selesai2', nama_institusi3='$nama_institusi3', jurusan3='$jurusan3', kota3='$kota3', tahun_mulai3='$tahun_mulai3', tahun_selesai3='$tahun_selesai3', nama_institusi4='$nama_institusi4', jurusan4='$jurusan4', kota4='$kota4', tahun_mulai4='$tahun_mulai4', tahun_selesai4='$tahun_selesai4', nama_institusi5='$nama_institusi5', jurusan5='$jurusan5', kota5='$kota5', tahun_mulai5='$tahun_mulai5', tahun_selesai5='$tahun_selesai5' WHERE id='$id'";

		if ($nama_institusi) {
			if (empty($kota)) {
				echo "<script>alert('Kota pada field pertama harap di isi!');history.go(-1)</script>";
			}elseif (strlen($kota) >= 16) {
				echo "<script>alert('Kota pada field pertama Maksimal 16 Karakter!');history.go(-1)</script>";
			}elseif (empty($tahun_mulai)) {
				echo "<script>alert('Tahun Mulai pada field pertama harap di isi!');history.go(-1)</script>";
			}elseif (!eregi("^[0-9]{4}$", $tahun_mulai)) {
				echo "<script>alert('Tahun Mulai pada field pertama salah!');history.go(-1)</script>";
			}elseif (empty($tahun_selesai)) {
				echo "<script>alert('Tahun Selesai pada field pertama harap di isi!');history.go(-1)</script>";
			}elseif (!eregi("^[0-9]{4}$", $tahun_selesai)) {
				echo "<script>alert('Tahun Selesai pada field pertama salah!');history.go(-1)</script>";
			}elseif ($nama_institusi2) {
				if (empty($kota2)) {
					echo "<script>alert('Kota pada field Ke 2 harap di isi!');history.go(-1)</script>";
				}elseif (strlen($kota2) >= 16) {
					echo "<script>alert('Kota pada field Ke 2 Maksimal 16 Karakter!');history.go(-1)</script>";
				}elseif (empty($tahun_mulai2)) {
					echo "<script>alert('Tahun Mulai pada field Ke 2 harap di isi!');history.go(-1)</script>";
				}elseif (!eregi("^[0-9]{4}$", $tahun_mulai2)) {
					echo "<script>alert('Tahun Mulai pada field Ke 2 salah!');history.go(-1)</script>";
				}elseif (empty($tahun_selesai2)) {
					echo "<script>alert('Tahun Selesai pada field Ke 2 harap di isi!');history.go(-1)</script>";
				}elseif (!eregi("^[0-9]{4}$", $tahun_selesai2)) {
					echo "<script>alert('Tahun Selesai pada field Ke 2 salah!');history.go(-1)</script>";
				}elseif ($nama_institusi3) {
					if (empty($kota3)) {
						echo "<script>alert('Kota pada field Ke 3 harap di isi!');history.go(-1)</script>";
					}elseif (strlen($kota3) >= 16) {
						echo "<script>alert('Kota pada field Ke 3 Maksimal 16 Karakter!');history.go(-1)</script>";
					}elseif (empty($tahun_mulai3)) {
						echo "<script>alert('Tahun Mulai pada field Ke 3 harap di isi!');history.go(-1)</script>";
					}elseif (!eregi("^[0-9]{4}$", $tahun_mulai3)) {
						echo "<script>alert('Tahun Mulai pada field Ke 3 salah!');history.go(-1)</script>";
					}elseif (empty($tahun_selesai3)) {
						echo "<script>alert('Tahun Selesai pada field Ke 3 harap di isi!');history.go(-1)</script>";
					}elseif (!eregi("^[0-9]{4}$", $tahun_selesai3)) {
						echo "<script>alert('Tahun Selesai pada field Ke 3 salah!');history.go(-1)</script>";
					}elseif ($nama_institusi4) {
						if (empty($kota4)) {
							echo "<script>alert('Kota pada field Ke 4 harap di isi!');history.go(-1)</script>";
						}elseif (strlen($kota4) >= 16) {
							echo "<script>alert('Kota pada field Ke 4 Maksimal 16 Karakter!');history.go(-1)</script>";
						}elseif (empty($tahun_mulai4)) {
							echo "<script>alert('Tahun Mulai pada field Ke 4 harap di isi!');history.go(-1)</script>";
						}elseif (!eregi("^[0-9]{4}$", $tahun_mulai4)) {
							echo "<script>alert('Tahun Mulai pada field Ke 4 salah!');history.go(-1)</script>";
						}elseif (empty($tahun_selesai4)) {
							echo "<script>alert('Tahun Selesai pada field Ke 4 harap di isi!');history.go(-1)</script>";
						}elseif (!eregi("^[0-9]{4}$", $tahun_selesai4)) {
							echo "<script>alert('Tahun Selesai pada field Ke 4 salah!');history.go(-1)</script>";
						}elseif ($nama_institusi5) {
							if (empty($kota5)) {
								echo "<script>alert('Kota pada field Ke 5 harap di isi!');history.go(-1)</script>";
							}elseif (strlen($kota5) >= 16) {
								echo "<script>alert('Kota pada field Ke 5 Maksimal 16 Karakter!');history.go(-1)</script>";
							}elseif (empty($tahun_mulai5)) {
								echo "<script>alert('Tahun Mulai pada field Ke 5 harap di isi!');history.go(-1)</script>";
							}elseif (!eregi("^[0-9]{4}$", $tahun_mulai5)) {
								echo "<script>alert('Tahun Mulai pada field Ke 5 salah!');history.go(-1)</script>";
							}elseif (empty($tahun_selesai5)) {
								echo "<script>alert('Tahun Selesai pada field Ke 5 harap di isi!');history.go(-1)</script>";
							}elseif (!eregi("^[0-9]{4}$", $tahun_selesai5)) {
								echo "<script>alert('Tahun Selesai pada field Ke 5 salah!');history.go(-1)</script>";
							}else{
								$hasil = mysql_query($query);
								echo "<script>alert('Data telah terupdate.');window.location='pendidikan_formal.php'; </script>";
							}
						}else{
							$hasil = mysql_query($query);
							echo "<script>alert('Data telah terupdate.');window.location='pendidikan_formal.php'; </script>";
						}
					}else{
						$hasil = mysql_query($query);
						echo "<script>alert('Data telah terupdate.');window.location='pendidikan_formal.php'; </script>";
					}
				}else{
					$hasil = mysql_query($query);
					echo "<script>alert('Data telah terupdate.');window.location='pendidikan_formal.php'; </script>";
				}
			}else{
				$hasil = mysql_query($query);
				echo "<script>alert('Data telah terupdate.');window.location='pendidikan_formal.php'; </script>";
			}
		}else{
			$hasil = mysql_query($query);
			echo "<script>alert('Data telah terupdate.');window.location='pendidikan_formal.php'; </script>";
		}
	}
		
	?>