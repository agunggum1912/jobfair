<!DOCTYPE html>
<html>
<head>
	<title>JobFair</title>
	<link rel="stylesheet" type="text/css" href="css/style_proses.css">
</head>
<body>

	<script type="application/javascript">
	//hanya boleh huruf
  	function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if ((charCode < 65) && charCode > 33)
            return false;         
         return true;
      }
	</script>
	<script type="application/javascript">
	//hanya boleh angka
  	function isNumberKeyTrue(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;         
         return true;
      }
	</script>

<?php 
include 'koneksi.php';
			
// mengaktifkan session
session_start();
 
// cek apakah user telah login, jika belum login maka di alihkan ke halaman login
if($_SESSION['status'] !="login"){
	header("location:index.php");
}
 
// menampilkan pesan selamat datang
echo "<div class='nav-bar'>";
echo 	"<ul>";
echo 		"<li class='dropdown'><a href='#'>".$_SESSION['email']."</a>";
echo 			"<ul class='isi-dropdown'>";
echo 				"<li><a href='logout.php'>"."Logout"."</a><li>";
echo 				"<li><a href='setting.php'>"."Setting"."</a><li>";
echo 			"</ul>";
echo 		"</li>";
echo 	"</ul>";
echo "</div>";

		$id = $_GET['id'];
		$query = "SELECT users.nama, users.no_hp, users.id, profil.tempat_lahir, profil.tgl_lahir, profil.jenis_kelamin, profil.alamat, profil.provinsi, profil.kota, profil.kecamatan, profil.kode_pos, profil.alamat_domisili, profil.kewarganegaraan, profil.agama, profil.status_perkawinan FROM profil INNER JOIN users ON profil.id = users.id WHERE email='$_SESSION[email]'";
		$hasil = mysql_query($query);
		$data = mysql_fetch_array($hasil);
		{
			// user
			$nama = $data ['nama'];
			$no_hp = $data ['no_hp'];
			// profil
			$tempat_lahir = $data ['tempat_lahir'];
			$tgl_lahir = $data ['tgl_lahir'];
			$alamat = $data ['alamat'];
			$kota = $data['kota'];
			$kecamatan = $data['kecamatan'];
			$provinsi = $data ['provinsi'];
			$kode_pos = $data ['kode_pos'];
			$kewarganegaraan = $data ['kewarganegaraan'];
			$alamat_domisili = $data ['alamat_domisili'];
			$jenis_kelamin = $data ['jenis_kelamin'];
			$agama = $data ['agama'];
			$status_perkawinan = $data ['status_perkawinan'];
		}

?>


<?php
	$strSQL = "SELECT users.id, users.email, users.nama, profil.id, profil.foto FROM users INNER JOIN profil ON users.id = profil.id WHERE email='$_SESSION[email]' ";
			$query = mysql_query ($strSQL) or die ("query salah");
?>

<div class="left_bar">
	<h3>JobFair</h3>
	<ul>
		<div class="foto_profil">
			<il>
				<?php
					while ($row = mysql_fetch_array($query)) {
						$cek_foto = $row ['foto'];
						$tempat_foto = 'foto/'.$row['foto']; 
						if ($cek_foto >= 1) {
							echo "<a href='setting.php'><img src='$tempat_foto'></a></br>";	
						}else{
							echo "<a href='setting.php'><img src='foto/blank.png'></a></br>";
						}
						echo "<a href='setting.php'>".$row ["nama"]."</a>";
					}
					?>	
			</il>
		</div>
	<il >
		<a class="menu" href="home.php">My Profile</a>
	</il>
	<li>
		<a class="menu2" href="pendidikan_formal.php">Formal Education</a>
	</li>
	<li>
		<a class="menu2" href="pendidikan_non_formal.php">Non Formal Education</a>
	</li>
	<li>
		<a class="menu2" href="pengalaman_kerja.php">Experience</a>
	</li>
	<li>
		<a class="menu2" href="pengalaman_organisasi.php">Organization Activity</a>
	</li>
	<li>
		<a class="menu2" href="keluarga.php">My Family</a>
	</li>
	
</div>
	<div class="hidden">Hidden</div>
<div class="tengah kotak_tengah">
	<table border="0">
		<tr>
			<td colspan="4">
				<div class="kotak_dalam">
					<center><b>Update My Profile</b></center>
				</div>
			</td>
		</tr>
		<div>
		
		
		<form name="profil_update_proses" method="post" action="profil_update_proses.php">
		
		<tr>
			<td colspan="4">
				<label>Nama Lengkap</label></br>
				<input required maxlength="32" class="textbox" type="text" name="nama" value="<?php echo $nama; ?>" onkeypress="return isNumberKey(event)" onPaste="true" autocomplete="off">
			</td>
		</tr>
		
		<tr>
			<td width="168">
				<label>No Handphone</label>
				<input required minlength="10" maxlength="13" class="textbox2" type="text" name="no_hp" value="<?php echo $no_hp; ?>" onkeypress="return isNumberKeyTrue(event)" onPaste="true" autocomplete="off">
			</td>
			<td width="168" colspan="2">
				<label>Tempat Lahir</label>
				<input required maxlength="20" class="textbox2" type="text" name="tempat_lahir" value="<?php echo $tempat_lahir; ?>">
			</td>
			<td width="168">
				<label>Tanggal Lahir</label>
				<input required class="textbox2" type="date" name="tgl_lahir" value="<?php echo $tgl_lahir; ?>" onkeypress="return isNumberKeyTrue(event)" onPaste="true" autocomplete="off">
			</td>
		</tr>
		
		<tr>
			<td colspan="4">
				<label>Alamat</label>
				<textarea required maxlength="150" class="textbox3" type="text" name="alamat"><?php echo $alamat; ?></textarea>
			</td>
		</tr>

		<tr>
			<td width="280" colspan="2">
				<label>Kota</label>
				<input required maxlength="16" class="textbox4" type="text" name="kota" value="<?php echo $kota; ?>">
			</td>
			<td width="280" colspan="2">
				<label>Provinsi</label>
				<input required maxlength="16" class="textbox4" type="text" name="provinsi" value="<?php echo $provinsi; ?>">
			</td>
		</tr>

		<tr>
			<td width="280" colspan="2">
				<label>Kode Pos</label>
				<input required maxlength="5" class="textbox4" type="text" name="kode_pos" value="<?php echo $kode_pos; ?>" onkeypress="return isNumberKeyTrue(event)" onPaste="true" autocomplete="off">
			</td>
			<td width="280" colspan="2">
				<label>Kewarganegaraan</label>
				<input required maxlength="12" class="textbox4" type="text" name="kewarganegaraan" value="<?php echo $kewarganegaraan; ?>">
			</td>
		</tr>
		
		<tr>
		 	<td colspan="4">
				<label>Alamat Domisili</label>
				<textarea required maxlength="150" class="textbox3" type="text" name="alamat_domisili"><?php echo $alamat_domisili; ?></textarea>
			</td>
		</tr>
		
		<tr>
			<td width="168"> 
				<label>Jenis Kelamin</label>
				<div>
					<select class="textbox5" required name="jenis_kelamin">
						<option><?php echo $jenis_kelamin ?></option>
						<option value="Laki-laki">Laki-laki</option>
						<option value="Perempuan">Perempuan</option>
					</select>
				</div>
			</td>
			<td width="168" colspan="2">
				<label>Status Perkawinan</label>
				<div>
					<select class="textbox5" required name="status_perkawinan">
						<option><?php echo $status_perkawinan ?></option>
						<option value="Menikah">Menikah</option>
						<option value="Belum Menikah">Belum Menikah</option>
						<option value="Cerai Hidup">Cerai Hidup</option>
						<option value="Cerai Mati">Cerai Mati</option>
					</select>
				</div>
			</td>
			<td width="168">
				<label>Agama</label>
				<div>
					<select class="textbox5" required name="agama">
						<option><?php echo $agama ?></option>
						<option value="Islam">Islam</option>
						<option value="Kristen">Kristen</option>
						<option value="Katolik">Katolik</option>
						<option value="Hindu">Hindu</option>
						<option value="Buddha">Buddha</option>
						<option value="Kong Hu Cu">Kong Hu Cu</option>
					</select>
				</div>
			</td>
			</tr>
				<?php $id= $data['id']; ?>
				<input required type="hidden" name="id" value=<?php echo $_GET['id'];?>>
			<tr>
				<td colspan="4"></br>
					<input class="button_update" type="submit" name="update" value="Update"> 
					<a class="button_back" href="home.php">Back</a>
				</td>
			</tr>

		</form>
		</div>
	</table>
	
</div>

</body>
</html>