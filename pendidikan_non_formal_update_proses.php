<?php
	include 'koneksi.php';

	if (isset($_POST['update'])) {

		$id = $_POST['id']; 
		$nama_institusi = $_POST['nama_institusi'];
		$tipe = $_POST['tipe'];
		$tgl_mulai = $_POST['tgl_mulai'];
		$tgl_selesai = $_POST['tgl_selesai'];
		$sertifikat = $_POST['sertifikat'];
		$nama_institusi2 = $_POST['nama_institusi2'];
		$tipe2 = $_POST['tipe2'];
		$tgl_mulai2 = $_POST['tgl_mulai2'];
		$tgl_selesai2 = $_POST['tgl_selesai2'];
		$sertifikat2 = $_POST['sertifikat2'];
		$nama_institusi3 = $_POST['nama_institusi3'];
		$tipe3 = $_POST['tipe3'];
		$tgl_mulai3 = $_POST['tgl_mulai3'];
		$tgl_selesai3 = $_POST['tgl_selesai3'];
		$sertifikat3 = $_POST['sertifikat3'];
		$nama_institusi4 = $_POST['nama_institusi4'];
		$tipe4 = $_POST['tipe4'];
		$tgl_mulai4 = $_POST['tgl_mulai4'];
		$tgl_selesai4 = $_POST['tgl_selesai4'];
		$sertifikat4 = $_POST['sertifikat4'];
		$nama_institusi5 = $_POST['nama_institusi5'];
		$tipe5 = $_POST['tipe5'];
		$tgl_mulai5 = $_POST['tgl_mulai5'];
		$tgl_selesai5 = $_POST['tgl_selesai5'];
		$sertifikat5 = $_POST['sertifikat5'];


		$query = "UPDATE pendidikan_non_formal SET nama_institusi='$nama_institusi', tipe='$tipe', tgl_mulai='$tgl_mulai', tgl_selesai='$tgl_selesai', sertifikat='$sertifikat', nama_institusi2='$nama_institusi2', tipe2='$tipe2', tgl_mulai2='$tgl_mulai2', tgl_selesai2='$tgl_selesai2', sertifikat2='$sertifikat2', nama_institusi3='$nama_institusi3', tipe3='$tipe3', tgl_mulai3='$tgl_mulai3', tgl_selesai3='$tgl_selesai3', sertifikat3='$sertifikat3', nama_institusi4='$nama_institusi4', tipe4='$tipe4', tgl_mulai4='$tgl_mulai4', tgl_selesai4='$tgl_selesai4', sertifikat4='$sertifikat4', nama_institusi5='$nama_institusi5', tipe5='$tipe5', tgl_mulai5='$tgl_mulai5', tgl_selesai5='$tgl_selesai5', sertifikat5='$sertifikat5' WHERE id='$id'";


		if (strlen($nama_institusi) >= 30) {
			echo "<script>alert('Nama Institusi pada field Pertama Maksimal 30 Karakter!');history.go(-1)</script>";
		}elseif ($nama_institusi) {
			if ($tipe != "KURSUS" && $tipe != "TRAINING" && $tipe != "WORKSHOP" && $tipe != "Lainnya") {
				echo "<script>alert('Tipe pada field pertama salah!');history.go(-1)</script>";
			}elseif (empty($tgl_mulai)) {
				echo "<script>alert('Tanggal Mulai pada field pertama harap di isi!');history.go(-1)</script>";
			}elseif (empty($tgl_selesai)) {
				echo "<script>alert('Tanggal Selesai pada field pertama harap di isi!');history.go(-1)</script>";
			}elseif ($sertifikat != "Bersertifikat" && $sertifikat != "Tidak Bersertifikat") {
				echo "<script>alert('Sertifikat pada field pertama salah!');history.go(-1)</script>";
			}elseif (strlen($nama_institusi2) >= 30) {
				echo "<script>alert('Nama Institusi pada field ke 2 Maksimal 30 Karakter!');history.go(-1)</script>";
			}elseif ($nama_institusi2) {
				if ($tipe2 != "KURSUS" && $tipe2 != "TRAINING" && $tipe2 != "WORKSHOP" && $tipe2 != "Lainnya") {
					echo "<script>alert('Tipe pada field ke 2 salah!');history.go(-1)</script>";
				}elseif (empty($tgl_mulai2)) {
					echo "<script>alert('Tanggal Mulai pada field ke 2 harap di isi!');history.go(-1)</script>";
				}elseif (empty($tgl_selesai2)) {
					echo "<script>alert('Tanggal Selesai pada field ke 2 harap di isi!');history.go(-1)</script>";
				}elseif ($sertifikat2 != "Bersertifikat" && $sertifikat2 != "Tidak Bersertifikat") {
					echo "<script>alert('Sertifikat pada field ke 2 salah!');history.go(-1)</script>";
				}elseif (strlen($nama_institusi3) >= 30) {
					echo "<script>alert('Nama Institusi pada field ke 3 Maksimal 30 Karakter!');history.go(-1)</script>";
				}elseif ($nama_institusi3) {
					if ($tipe3 != "KURSUS" && $tipe3 != "TRAINING" && $tipe3 != "WORKSHOP" && $tipe3 != "Lainnya") {
						echo "<script>alert('Tipe pada field ke 3 salah!');history.go(-1)</script>";
					}elseif (empty($tgl_mulai3)) {
						echo "<script>alert('Tanggal Mulai pada field ke 3 harap di isi!');history.go(-1)</script>";
					}elseif (empty($tgl_selesai3)) {
						echo "<script>alert('Tanggal Selesai pada field ke 3 harap di isi!');history.go(-1)</script>";
					}elseif ($sertifikat3 != "Bersertifikat" && $sertifikat3 != "Tidak Bersertifikat") {
						echo "<script>alert('Sertifikat pada field ke 3 salah!');history.go(-1)</script>";
					}elseif (strlen($nama_institusi4) >= 30) {
						echo "<script>alert('Nama Institusi pada field ke 4 Maksimal 30 Karakter!');history.go(-1)</script>";
					}elseif ($nama_institusi4) {
						if ($tipe4 != "KURSUS" && $tipe4 != "TRAINING" && $tipe4 != "WORKSHOP" && $tipe4 != "Lainnya") {
							echo "<script>alert('Tipe pada field ke 4 salah!');history.go(-1)</script>";
						}elseif (empty($tgl_mulai4)) {
							echo "<script>alert('Tanggal Mulai pada field ke 4 harap di isi!');history.go(-1)</script>";
						}elseif (empty($tgl_selesai4)) {
							echo "<script>alert('Tanggal Selesai pada field ke 4 harap di isi!');history.go(-1)</script>";
						}elseif ($sertifikat4 != "Bersertifikat" && $sertifikat4 != "Tidak Bersertifikat") {
							echo "<script>alert('Sertifikat pada field ke 4 salah!');history.go(-1)</script>";
						}elseif (strlen($nama_institusi5) >= 30) {
							echo "<script>alert('Nama Institusi pada field ke 5 Maksimal 30 Karakter!');history.go(-1)</script>";
						}elseif ($nama_institusi5) {
							if ($tipe5 != "KURSUS" && $tipe5 != "TRAINING" && $tipe5 != "WORKSHOP" && $tipe5 != "Lainnya") {
								echo "<script>alert('Tipe pada field ke 5 salah!');history.go(-1)</script>";
							}elseif (empty($tgl_mulai5)) {
								echo "<script>alert('Tanggal Mulai pada field ke 5 harap di isi!');history.go(-1)</script>";
							}elseif (empty($tgl_selesai5)) {
								echo "<script>alert('Tanggal Selesai pada field ke 5 harap di isi!');history.go(-1)</script>";
							}elseif ($sertifikat5 != "Bersertifikat" && $sertifikat5 != "Tidak Bersertifikat") {
								echo "<script>alert('Sertifikat pada field ke 5 salah!');history.go(-1)</script>";
							}else{
								$hasil = mysql_query($query);
								echo "<script>alert('Data telah terupdate.');window.location='pendidikan_non_formal.php'; </script>";
							}
						}else{
							$hasil = mysql_query($query);
							echo "<script>alert('Data telah terupdate.');window.location='pendidikan_non_formal.php'; </script>";
						}
					}else{
						$hasil = mysql_query($query);
						echo "<script>alert('Data telah terupdate.');window.location='pendidikan_non_formal.php'; </script>";
					}
				}else{
					$hasil = mysql_query($query);
					echo "<script>alert('Data telah terupdate.');window.location='pendidikan_non_formal.php'; </script>";
				}
			}else{
				$hasil = mysql_query($query);
				echo "<script>alert('Data telah terupdate.');window.location='pendidikan_non_formal.php'; </script>";
			}
		}
	}
		
	?>