<!DOCTYPE html>
<html>
<head>
	<title>JobFair</title>
	<link rel="stylesheet" type="text/css" href="css/style_register.css">
</head>
<body>
	<div class="top_bar">hidden</div>

	<script type="application/javascript">
	//hanya boleh huruf
  	function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if ((charCode < 65) && charCode > 33)
            return false;         
         return true;
      }
	</script>
	<script type="application/javascript">
	//hanya boleh angka
  	function isNumberKeyTrue(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;         
         return true;
      }
	</script>

	<div class="tengah kotak_tengah">
		<h2>REGISTER FOR AN ACCOUNT</h2>

		<form action="registerproses.php" method="post" name="register">
	<div class="kotak_dalam">
		<label>Full Name</label></br>
		<div>
			<input required class="textbox" type="text" name="full_name" onkeypress="return isNumberKey(event)" onPaste="true" >
		</div>
		<label>Phone Number</label></br>
		<div>
			<input required class="textbox" type="text" name="phone_number" onkeypress="return isNumberKeyTrue(event)" minlength="10" maxlength="13">
		</div>
		<label>Email</label></br>
		<div>
			<input required class="textbox" type="email" name="email" autocomplete="off">
		</div>
		<label>Password</label></br>
		<div>
			<input required class="textbox" type="password" name="password" minlength="6" maxlength="16">
		</div>
		<label>Confirm Password</label></br>
		<div>
			<input required class="textbox" type="password" name="confirm_password" minlength="6" maxlength="16">
		</div>
		<p><input required type="checkbox" name="check"> Dengan ini saya menyetujui aturan dan penggunaan dan kebijakan yang berlaku.</p></br>
		
		<input class="button_register" type="Submit" name="register" value="Register">
		<a class="button_back" href="index.php">Back</a>
	
	</div>
		</form>

	</div>
</body>
</html>