<!DOCTYPE html>
<html>
<head>
	<title>JobFair</title>
		<link rel="stylesheet" type="text/css" href="css/style_index.css">
</head>
<body>
	<div class="top_bar">hidden</div>

	<div class="tengah kotak_tengah">
		<h2>Login to Your Account</h2>
		<form action="login.php" method="post" name="login" onSubmit="return validasi()">
			<div class="kotak_dalam">
				<div>
					<label>Email:</label>
					</br>
					<input class="textbox" type="text" name="email" id="email" />
				</div>
				<div>
					<label>Password:</label>
					</br>
					<input class="textbox" type="password" name="password" id="password" autocomplete="off" />
				</div>	
				<div>
					<input class="button_login" name="login" type="submit" value="LOGIN" class="tombol">
				</div>
					<center class="tulisan_or">or</center>
					<p class="box_or">_____________________________________</p>
					</br>
					<a class="button_register" href="register.php">Register</a>
				</div>
		</form>
	</div>


<script type="text/javascript">
	function validasi() {
		var email = document.getElementById("email").value;
		var password = document.getElementById("password").value;		
		if (email != "" && password!="") {
			return true;
		}else{
			alert('Email dan Password harus di isi !');
			return false;
		}
	}
 
</script>

</body>
</html>